#include <stdio.h>
#include <stdlib.h>

// Thibault Desprez 2018

enum Color{noir,rouge,vert,orange,bleu,violet,cyan,blanc};

void setFontColor(unsigned int color){
	if (color < 0 || color > 7 )
		return;
	printf("\033[3%dm",color);
}

void setBackgroundColor(unsigned int color){
	if (color < 0 || color > 7 )
		return;
	printf("\033[4%dm",color);
}

void setCosoleColor(int fg , int bg){
	setBackgroundColor(bg);
	setFontColor(fg); 
}

void resetConsoleColor(){
	printf("\033[0m");
}


int main(int argc, char const *argv[])
{	
	printf("   ");
	setBackgroundColor(rouge);
	printf("   ");
	resetConsoleColor();
	printf("\n");
	for (int j = 0; j < 3; ++j)
	{
		resetConsoleColor();
		printf("   ");
		setBackgroundColor(orange);
		for (int i = 0; i < 3; ++i)
		{
			printf(" ");
		}
		resetConsoleColor();
		printf("\n");
	}


	printf("  ");
	setBackgroundColor(orange);
	for (int i = 0; i < 5; ++i)
	{
		printf(" ");
	}
	resetConsoleColor();
	printf("  \n");




	for (int i = 0; i < 4; ++i)
	{
		resetConsoleColor();
		printf(" ");
		for (int j = 0; j < 7; ++j)
		{
			setBackgroundColor(orange);
			printf(" ");
		}
		resetConsoleColor();
		printf(" \n");
	}
	printf(" ");
	setFontColor(noir);
	setBackgroundColor(blanc);
	printf("JUPYLER");
	resetConsoleColor();
	printf("\n");

	for (int i = 0; i < 4; ++i)
	{
		resetConsoleColor();
		printf(" ");
		for (int j = 0; j < 7; ++j)
		{
			setBackgroundColor(orange);
			printf(" ");
		}
		resetConsoleColor();
		printf(" ");
		printf("\n");
	}
	return 0;
}
