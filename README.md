# `mkreports`

## 1) Fichier de configuration

- Extension `.py` dans le répertoire courant
- Modifier promo / date
- Mettre `pretend` à `True` pour tester (ne sort que le premier bilan)
- Tout le reste à `False` avant de les réactiver un par un
- Nom du fichier de sortie

## 2) Fichier infos

- Sauvergarder le fichier `xlsx` en `csv` UTF8
- Renseigner nom `infos_csv`
- Nom colonnes pour nom + prénom

## 3) Fichier de notes

- Sauvegarde résultat calcul Aurion en `.csv`
- `utf8_conv file.csv` pour convertir en UTF8
- Mettre le bon nom à `grades_csv`
- Vérifier UE + ECTS w/ règlement études dans `competences`
- Noms de colonnes correspondant au `csv`
- Renommer colonnes récupérées d'une année précédente si nécessaire
- `mkreports` pour tester
- `UNDEFINED` : attention, peut-être mauvais séparateur de colonnes
- `ects` et `grades` à `True` si souhaités

## 4) Photos

- `picture: True`
- Dossier `img` (ou lien) contenant les photos nommées `id.jpg`
- Ajuster `vertical_dilatation` pour que l'histogramme soit beau

## 5) Absences

- Aurion : Planning -> Assiduité puis mettre le bon groupe + dates
- N'afficher que les absences excusées + sauvegarde csv
- Idem avec les absences non excusées
- `utf8_conv` les deux fichiers
- `attendance: True`

## 6) Autres infos

- En fonction du fichier infos
- Doublant et LV2 par défaut
- Renseigner les titres de colonnes
- Puis `pretend: False` et vérifier que tout est bon

# awèye

- php-mail php-mail-mime html2text msmtp-mta
- sudo wget https://raw.githubusercontent.com/mtibben/html2text/master/src/Html2Text.php dans /usr/share/php
