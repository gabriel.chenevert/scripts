#!/usr/bin/python3
# coding: utf-8
#
# Petites fonctions utilitaires partagées par plusieurs scripts
#
# GCH 2015/07/03 petites modifs
# GCH 2015/02/18 v.1

def num( s, sep = "," ):

  s = safe(s)

  if s in ["", "---", "X", "Indisponibilite_excusee_avec_justificatif", "Absence_non_excusee"]:
    s = "0"
    
  s = "%.2f" % float(s)
  s = s.replace(".", sep)
  s = s.replace(",", sep)
  
  return s

def safe( s, strip_dot = False ):

  dic = {" ": "_", ",": ".", "ç": "c", "É": "E", "é": "e", "è": "e", "ë": "e", "ë": "e", "î": "i", "ï": "i", "ô": "o"}

  if strip_dot:
    s = s.replace(".", "")
    
  for i, j in dic.items():
    s = s.replace(i, j)

  return s

def rep( s, vars, data ):

  if s in vars:
    return data[vars.index(s)]
  else:
    print("Warning: %s undefined" % s)
    return "UNDEFINED"

def mul_rep( s, vars, data ):
  
  for v in zip(vars,data):
    
    if v[0]: s = s.replace(v[0], v[1])
    
  return s
